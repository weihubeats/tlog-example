package com.example.customlabel;

import com.example.customlabel.vo.Person;
import com.yomahub.tlog.core.annotation.TLogAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TestDomain2 {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @TLogAspect("id")
    public void testMethod(long id){
        log.info("this is long value,{}",id);
    }
}
