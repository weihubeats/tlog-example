package com.yomahub.tlog.example.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunnerApplication {

/*    static {
        XxlJobEnhance.enhance();
    }*/
    public static void main(String[] args) {
        SpringApplication.run(RunnerApplication.class, args);
    }

}
